i18n = i18n or {}
i18n.DefaultLang = "en"
i18n.Lang = i18n.Lang or {}

--[[

    Usage :
        local LANG = i18n:CreateLanguage( "en", "English" )
        LANG:AddPhrase( "Phrase1", "Hello, I'm an English phrase" )
        LANG:AddPhrase( "Phrase2", "Hello, I'm another English phrase" )

        local LANG = i18n:CreateLanguage( "fr", "French" )
        LANG:AddPhrase( "Phrase1", "Bonjour, je suis une phrase en français" )
        LANG:AddPhrase( "Phrase2", "Bonjour, je suis une autre phrase en français" )

        i18n:Get( "Phrase1", "fr" )     -- output : "Bonjour, je suis une phrase en français"
        i18n:Get( "Phrase2" )           -- output : "Hello, I'm another English phrase" -- (no lang specified, i18n.DefaultLang will be used)

]]--

-- Credits: https://stackoverflow.com/a/26367080

local function copy( tOriginal, tSeen )
    if ( type( tOriginal ) ~= "table" ) then
        return tOriginal
    end

    if tSeen and tSeen[ tOriginal ] then
        return tSeen[ tOriginal ]
    end

    local tSeen = tSeen or {}
    local tCopied = setmetatable( {}, getmetatable( tOriginal ) )

    tSeen[ tOriginal ] = tCopied

    for k, v in pairs( tOriginal ) do
        tCopied[ copy( k, tSeen ) ] = copy( v, tSeen )
    end

    return tCopied
end

--[[

    LANG:AddPhrase
    Desc: Adds or overrides a phrase in a language object

]]--

local oLang = {
    Name = "Default",
    Phrase = {}
}

function oLang:AddPhrase( xKey, sPhrase )
    if not xKey or not sPhrase then
        return
    end

    self.Phrase[ xKey ] = sPhrase
end

--[[

    i18n:CreateLanguage
    Desc: Create a new language, and return it's tOriginalect

]]--

function i18n:CreateLanguage( sID, sName )
    if not sID then
        return
    end

    local tLang = copy( oLang )
    tLang.Name = ( sName or sID )

    self.Lang[ string.lower( sID ) ] = tLang

    return tLang
end

--[[

    i18n:Get
    Desc: Return Phrase from the current language, or from a specified one

]]--

function i18n:Get( sKey, sLang )
    local sLang = ( sLang or self.DefaultLang )

    if not sLang or not self.Lang[ sLang ] then
        return "[Language missing: " .. sLang .. "]"
    end

    return self.Lang[ sLang ].Phrase[ sKey ] or ( "[String missing: " .. sKey .. "]" )
end